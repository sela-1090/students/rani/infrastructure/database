# PostgreSQL Database Deployment on Kubernetes (AKS)

This repository contains the necessary files to deploy a PostgreSQL database on a Kubernetes cluster, specifically using Azure Kubernetes Service (AKS). The deployment includes a PersistentVolume (PV), a PersistentVolumeClaim (PVC), a Secret for storing database credentials securely, and a StatefulSet to manage the PostgreSQL pods.

## Dockerfile

The `Dockerfile` provided in this repository uses the official PostgreSQL image from DockerHub (`postgres:latest`) as the base image. It also copies SQL files from the local directory to the `/docker-entrypoint-initdb.d/` directory within the container. These SQL files will be automatically executed when the container starts, initializing the PostgreSQL database with the specified data.

## Jenkinsfile
Update values and push to argo repo

## Kubernetes Manifest (db-deployment.yaml)

The `db-deployment.yaml` file contains the Kubernetes manifests to create the required resources for the PostgreSQL deployment. Here's a breakdown of the components:

### PersistentVolumeClaim (PVC)

### Secret

### StatefulSet

### Service

## Deployment Instructions

To deploy the PostgreSQL database on your Kubernetes cluster :

- Apply the `db-deployment.yaml` file to the cluster:
   ```bash
   kubectl apply -f db-deployment.yaml -n database
   ```

This will create the necessary resources to deploy PostgreSQL in the "database" namespace and make it accessible via the NodePort service.

## Helm Chart 
### Containes the db-deployemnt componenets with values file for DB Deployment on Argo using HELM

Configuration
The following parameters can be customized in values.yaml:

dbName: Base64-encoded value of the PostgreSQL database name.
dbUser: Base64-encoded value of the PostgreSQL username.
dbPassword: Base64-encoded value of the PostgreSQL password.
replicaCount: Number of StatefulSet replicas.
image: Docker image for PostgreSQL.
imagePullPolicy: Image pull policy for the container.
resources: Resource requests for CPU and memory.
persistence.enabled: Enable/disable PersistentVolumeClaims.
persistence.size: Size of the PersistentVolumeClaims.

### Deploying with Helm
helm install database helm-chart/ --namespace database